/*
 * These are the JavaScript functions integrating AcronymMaker into the web application.
 *
 * Copyright (c) 2021 - Romain Wallon
 */

/* -------------- *
 * Initialization *
 * -------------- */

/**
 * Initializes the dynamic components of the web application.
 */
function initDocument() {
    loadDictionaries();
}

/**
 * Initializes the dictionaries used as acronym sources by AcronymMaker.
 */
function loadDictionaries() {
    const select = $('#dictionary');
    for (const dictionaryName of Object.keys(languageDictionaries)) {
        const [language, variant] = dictionaryName.split('-');
        const option = `<option value="${dictionaryName}">${language[0].toUpperCase() + language.slice(1)} (${variant})</option>`;
        select.append($(option));
    }
}

/* ------------- *
 * Token Builder *
 * ------------- */

/**
 * Adds a new token to the token builder.
 */
function addToken() {
    // First, computing the identifier of the new token.
    let tokenId = 1;
    const tokens = $('#tokens .token');
    if (tokens.length > 0) {
        const lastId = $(tokens[tokens.length - 1]).attr('id');
        tokenId = Number(lastId.split('-')[1]) + 1;
    }

    // Creating and adding the new token.
    const token = `
        <div id="token-${tokenId}" class="align-self-center p-2 card token">
            <div class="card-body container">
                <div class="text-center card-link custom-control custom-switch">
                    <input id="optional-${tokenId}" class="custom-control-input optional-token" type="checkbox">
                    <label class="custom-control-label" for="optional-${tokenId}">Optional</label>
                </div>
                <div class="input-group mb-3">
                    <input type="text" class="form-control token-word" title="A word of the token">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" onclick="deleteWord(this)">
                            Delete
                        </button>
                    </div>
                </div>
                <a class="card-link text-primary" onclick="addWord(this)">Add word</a>
                <a class="card-link text-secondary" onclick="deleteToken(this)">Delete</a>
            </div>
        </div>
    `;
    $("#tokens").append($(token));
}

/**
 * Deletes the token associated to the given button.
 *
 * @param button The button clicked to delete the token.
 */
function deleteToken(button) {
    const token = button.parentNode.parentNode;
    token.parentNode.removeChild(token);
}

/**
 * Adds a word to the token associated to the given button.
 *
 * @param button The button clicked to add a word.
 */
function addWord(button) {
    const word = `
        <div class="input-group mb-3">
            <input type="text" class="form-control token-word" title="A word of the token">
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button" onclick="deleteWord(this)">
                    Delete
                </button>
            </div>
        </div>
    `;
    $(word).insertBefore($(button));
}

/**
 * Deletes the word associated to the given button.
 *
 * @param button The button clicked to delete the word.
 */
function deleteWord(button) {
    const word = button.parentNode.parentNode;
    word.parentNode.removeChild(word);
}

/* ----------------- *
 * Table of Acronyms *
 * ----------------- */

/**
 * Adds the explanations of the given acronym to the table of acronyms.
 * This function also makes sure the browser displays immediately the new explanations.
 *
 * @param acronym The acronym to add the explanations of.
 */
function addExplanationsOf(acronym) {
    const word = acronym.getWord();
    for (const explanation of acronym.getExplanations()) {
        setTimeout(() => addExplanation(word, explanation), 50);
    }
}

/**
 * Adds an explanation of the given acronym to the table of acronyms.
 *
 * @param acronym The explained acronym.
 * @param explanation The explanation of the acronym.
 */
function addExplanation(acronym, explanation) {
    const acronymRow = `
        <tr>
            <th scope="row">${acronym}</th>
            <td>${explanation}</td>
            <td>
                <a class="text-primary" onclick="copyAcronym(this)">Copy to clipboard</a>
            </td>
            <td>
                <a class="text-secondary" onclick="deleteAcronym(this)">Delete</a>
            </td>
        </tr>
    `;
    $('#acronym-results tbody').append($(acronymRow));
}

/**
 * Copies the acronym associated to the given button to the clipboard.
 *
 * @param button The button clicked for copying the acronym.
 */
function copyAcronym(button) {
    const row = button.parentNode.parentNode;
    const acronym = row.children[0].textContent + ' - ' + row.children[1].textContent;
    navigator.clipboard.writeText(acronym).then(function() {}, function() {
        alert('Sorry, could not copy acronym to clipboard 😢');
    });
}

/**
 * Deletes the acronym associated to the given button.
 *
 * @param button The button clicked for deleting the acronym.
 */
function deleteAcronym(button) {
    const row = button.parentNode.parentNode;
    row.parentNode.removeChild(row);
}

/* -------------- *
 * Acronym Search *
 * -------------- */

/**
 * Gives the content of the dictionaries that have been selected.
 *
 * @return The array of selected dictionaries.
 *
 * @throws {Error} If no dictionary is selected.
 */
function getDictionaries() {
    // Collecting dictionaries.
    const dictionaries = [];
    for (const option of $('#dictionary option')) {
        if (option.selected) {
            dictionaries.push(languageDictionaries[$(option).attr('value')]);
        }
    }

    // Checking the validity of the dictionaries.
    if (dictionaries.length === 0) {
        throw Error('You must select a dictionary as source for acronyms.');
    }

    return dictionaries;
}

/**
 * Reads the value of a number having the given identifier.
 *
 * @param id The identifier of the value to read.
 *
 * @return The value of the number.
 *
 * @throws {Error} If the value is not a positive number.
 */
function readNumber(id) {
    const value = Number($(id)[0].value);
    if (value < 0) {
        throw Error('Only positive values may be used to limit the number of unexplained letters.');
    }
    return value;
}

/**
 * Gives the options specified by the user for the configuration of AcronymMaker,
 * based on the values set to the different components of the web application.
 *
 * @return The configuration for AcronymMaker.
 *
 * @throws {Error} If the configuration is invalid.
 */
function getOptions() {
    return {
        'select-letters': $('#first-letters')[0].checked ? 'first' : 'all',
        'matching-strategy': $('#unordered')[0].checked ? 'unordered' : 'ordered',
        'max-consecutive-unused': readNumber('#max-consecutive'),
        'max-total-unused': readNumber('#max-total'),
        'dictionary': getDictionaries()
    };
}

/**
 * Reads the tokens from a raw input field.
 *
 * @param tokens The input field in which the tokens are specified.
 *
 * @return The raw token string.
 *
 * @throws {Error} If there are no specified tokens.
 */
function readTokens(tokens) {
    const rawTokens = tokens.value.trim();
    if (rawTokens.length === 0) {
        throw Error('You must specify at least one token.');
    }
    return rawTokens;
}

/**
 * Builds the token described in the given card.
 *
 * @param tokenCard The card to build the token from.
 *
 * @return The built token.
 */
function buildToken(tokenCard) {
    // Checking whether the token is optional.
    const optionalToggle = $(tokenCard).find('.optional-token');
    const optional = optionalToggle[0].checked;

    // Collecting the words of the token.
    const words = [];
    for (const word of $(tokenCard).find('.token-word')) {
        const wordValue = word.value.trim();
        if (wordValue.length > 0) {
            words.push(wordValue);
        }
    }

    return (words.length === 0) ? null : { optional: optional, words: words };
}

/**
 * Builds the tokens described in the token builder.
 *
 * @returns The array of built tokens.
 *
 * @throws {Error} If there are no specified tokens.
 */
function buildTokens() {
    // Building each individual token.
    const tokens = [];
    for (const tokenCard of $('#tokens .token')) {
        const token = buildToken(tokenCard);
        if (token !== null) {
            tokens.push(token);
        }
    }

    // Checking the validity of the tokens.
    if (tokens.length === 0) {
        throw Error('You must specify at least one non-empty token.');
    }

    return tokens;
}

/**
 * Collects the settings specified by the user and searches for acronyms
 * based on these settings.
 */
function searchAcronyms() {
    try {
        // Removing the previous acronyms
        $('#acronym-results tbody').empty();

        // Collecting the user's input.
        const maker = createAcronymMaker(getOptions(), addExplanationsOf);
        const tokens = $('input#tokens');

        if (tokens.length > 0) {
            // Tokens are specified as raw input.
            maker.findAcronymsForString(readTokens(tokens[0]));

        } else {
            // Tokens are specified with the token builder.
            maker.findAcronymsForObjects(buildTokens());
        }

    } catch (e) {
        // An error as occurred: telling the user what is wrong.
        alert(e.message);
    }
}
