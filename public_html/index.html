<!DOCTYPE html>

<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">

        <meta name="theme-color" content="#157878">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="author" content="Romain Wallon">

        <meta name="description" content="Create awesome acronyms for your projects!">
        <meta name="keywords" content="acronym acronyms build create make word words">

        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/cayman.css">
        <link rel="stylesheet" type="text/css" href="css/highlight.css">
        <link rel="stylesheet" type="text/css" href="css/normalize.css">
        <link rel="stylesheet" type="text/css" href="css/acronymmaker.css">

        <title>AcronymMaker - Create awesome acronyms for your projects!</title>
    </head>

    <body>
        <section class="page-header">
            <h1 class="project-name">AcronymMaker</h1>

            <h2 class="project-tagline">Create awesome acronyms for your projects!</h2>

            <a class="btn-cayman" href="https://www.npmjs.com/package/acronymmaker" target="_blank">Node.js Module</a>
            <a class="btn-cayman" href="https://pypi.org/project/acronymmaker" target="_blank">Python Library</a>
            <a class="btn-cayman" href="https://gitlab.com/acronym-maker" target="_blank">GitLab Repositories</a>
        </section>

        <section class="main-content">

            <h2 id="description">Description</h2>

            <p>
                Let us briefly describe how <em>AcronymMaker</em> works with
                some vocabulary.
                If you want to skip this description, you may also directly
                <a href="#enter-tokens">enter your tokens</a>.
            </p>

            <p>
                A <em>token</em> is a set of words from which one word must appear
                in the acronym built by <em>AcronymMaker</em>.
                Said differently, there must be a letter in common between a word
                from the set and the built acronym.
                This letter may be either the first letter of a word in the token
                or any letter, depending on the <em>letter selection strategy</em>
                that is given to <em>AcronymMaker</em>.
            </p>

            <p>
                Additionally, tokens may be <em>optional</em>.
                In this case, <em>AcronymMaker</em> will try to match a letter from
                the words in the optional token to a letter in the acronym, but the
                acronym will still be accepted if it fails to do so.
            </p>

            <p>
                To find an acronym for a given sequence of tokens, <em>AcronymMaker</em>
                uses a <em>dictionary</em>, i.e., a set of known words, in which it looks
                for acronyms.
                A word in the dictionary is said to be <em>explained</em> (as an acronym)
                by the sequence of tokens if there is a letter in the word for at least
                one word in each (non-optional) token.
                In this case, we say that the letter is <em>explained</em> by the
                corresponding word.
            </p>

            <p>
                Moreover, there are two ways to explain a word as an acronym: either
                by following the order of the tokens in the specified sequence, or
                without considering this order.
                <em>AcronymMaker</em> supports both of them (independently).
            </p>

            <p>
                Finally, note that there may be unexplained letters in the acronym.
                Their number may be limited, by limiting both the number of consecutive
                unused letters and the number of overall unused letters in a word.
                If one of these limits is exceeded, then the word will not be considered
                as explained.
            </p>

            <p class="text-center">
                <b>It is now time to create awesome acronyms!</b>
            </p>

            <h2 id="enter-tokens">1. Enter your Tokens</h2>

            <p>
                Please specify your tokens in the field below.
                The words of a token must be separated by a slash (<code>/</code>), and
                optional tokens must be marked by an ending question mark (<code>?</code>).
                Each token must be separated by a white space.
                For instance, write <code>foo/bar? baz</code> if you want to find acronyms
                explaining either <code>foo baz</code>, <code>bar baz</code> or <code>baz</code>.
            </p>

            <p>
                There is no word limit, but keep in mind that specifying many words may
                slow down the search for acronyms (especially because acronyms are
                computed by your browser).
            </p>

            <form>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="tokens">Your tokens:</label>
                    <div class="col-sm-10">
                        <input id="tokens" class="form-control" type="text" placeholder="foo/bar? baz">
                    </div>
                </div>
            </form>

            <p>
                Note that we also provide a <a href="index-easy.html#enter-tokens">token builder</a>
                if you prefer to be guided for this step.
            </p>

            <h2 id="settings">2. Select your Settings</h2>

            <p>
                You may now customize how your acronyms will be created, based on the different
                variants described <a href="#description">above</a>.
                First, check the following toggles if you want to alter the default behavior
                of <em>AcronymMaker</em>.
            </p>

            <form>
                <div class="form-group row">
                    <div class="col-sm text-center custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="first-letters">
                        <label class="custom-control-label" for="first-letters">Only use initial letters from the tokens</label>
                    </div>
                    <div class="col-sm text-center custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="unordered">
                        <label class="custom-control-label" for="unordered">Do not preserve token order</label>
                    </div>
                </div>
            </form>

            <p>
                Now, you may specify how many unexplained letters may appear in the found acronyms.
            </p>

            <form>
                <div class="form-group row">
                    <label class="col-sm-6 col-form-label" for="max-consecutive">Maximum consecutive unexplained letters:</label>
                    <div class="col-sm-6">
                        <input id="max-consecutive" class="form-control" type="number" value="3">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-6 col-form-label" for="max-total">Maximum overall unexplained letters:</label>
                    <div class="col-sm-6">
                        <input id="max-total" class="form-control" type="number" value="5">
                    </div>
                </div>
            </form>

            <p>
                Finally, choose from which dictionary acronyms must be looked for.
            </p>

            <form>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label" for="dictionary">Dictionary:</label>
                    <div class="col-sm-10">
                        <select id="dictionary" class="custom-select"></select>
                    </div>
                </div>
            </form>

            <h2 id="acronyms">3. Choose your Acronyms</h2>

            <p>
                You are now ready to search for acronyms!
                Press the button below to start.
            </p>

            <div class="text-center">
                <button type="button" class="btn btn-primary" onclick="searchAcronyms()">Search for Acronyms</button>
            </div>

            <div class="table-responsive">
                <table id="acronym-results" class="table table-hover mx-auto">
                    <tbody></tbody>
                </table>
            </div>

            <footer class="site-footer">
                <span class="site-footer-owner">
                    This web application is maintained by <a href="http://www.lix.polytechnique.fr/~wallon/en" target="_blank">Romain Wallon</a>.
                </span>
                <span class="site-footer-credits">
                    It is based on the
                    <a href="http://github.com/jasonlong/cayman-theme" target="_blank">Cayman theme</a>
                    by <a href="http://github.com/jasonlong" target="_blank">Jason Long</a>.
                </span>
            </footer>
        </section>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="js/highlight.pack.js"></script>
        <script src="js/dictionaries.js"></script>
        <script src="js/acronymmaker.js"></script>
        <script src="js/acronymmaker-web.js"></script>

        <script>hljs.initHighlightingOnLoad();</script>
        <script>$(document).ready(initDocument);</script>
    </body>
</html>
