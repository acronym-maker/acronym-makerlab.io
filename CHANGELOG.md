# Changelog

This file describes the evolution of the web application of *AcronymMaker*.

## Version 0.1.0 (February 2021)

+ Provides a web interface for the version 0.1.1 of *AcronymMaker*, released
  as a [*Node.js* module](https://www.npmjs.com/package/acronymmaker).
+ Provides a token builder or a raw input for entering the tokens to find
  acronyms for.
+ Allows switching between the different strategies implemented in
  *AcronymMaker*.
+ Allows selecting different dictionaries as acronym sources.
+ Provides an interactive display of the found acronyms.
