# AcronymMaker Web Application

[![pipeline status](https://gitlab.com/acronym-maker/acronym-maker.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/acronym-maker/acronym-maker.gitlab.io/commits/master)

## Description

This project aims at developing a web application for *AcronymMaker* to
create awesome acronyms for your projects.
The application is available [here](https://acronym-maker.gitlab.io).

Note that this application is mostly a web interface for the [*Node.js* module
of *AcronymMaker*](https://www.npmjs.com/package/acronymmaker).
You may thus be also interested in having a look to the
[GitLab project](https://gitlab.com/acronym-maker/javascript-implementation)
of this module.
