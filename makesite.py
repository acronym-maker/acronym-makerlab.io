#!/usr/bin/env python3

from glob import glob
from os import chdir, path, system
from shutil import copyfile

#############
# FUNCTIONS #
#############

def submodule_update() -> None:
    """
    Updates the Git submodules of this project, so as to get the full
    library of AcronymMaker.
    """
    system('git submodule update --remote --init')

def browserify(dest_file: str) -> None:
    """
    Builds a browser-friendly version of the JavaScript implementation of AcronymMaker.

    :param dest_file: The path of the file in which to store the browser-friendly version
                      of AcronymMaker, relative to the root of the project.
    """
    chdir('javascript-implementation')
    system('npm install --save-dev')
    system('npm run browserify')
    chdir('..')
    copyfile('javascript-implementation/build/acronymmaker.js', dest_file)

def create_dictionaries(dest_file: str) -> None:
    """
    Builds the dictionaries recognized by AcronymMaker.

    :param dest_file: The path of the file in which to store the dictionaries,
                      relative to the root of the project.
    """
    all_dictionaries = {}
    for file_path in glob('dictionaries/*/*.txt'):
        with open(file_path) as file:
            language = path.basename(path.dirname(file_path))
            variant = path.basename(file_path)[:-4]
            all_dictionaries[f'{language}-{variant}'] = [line.strip() for line in file.readlines()]

    with open(dest_file, 'w') as file:
        print('/* Dictionaries used by AcronymMaker as sources of acronyms. */', file=file)
        print('const languageDictionaries = ', all_dictionaries, ';', sep='', file=file)

########
# MAIN #
########

if __name__ == '__main__':
    print('Updating Git submodules...')
    submodule_update()
    print('Done!')
    print()

    print('Browserifying AcronymMaker...')
    browserify('public_html/js/acronymmaker.js')
    print('Done!')
    print()

    print('Creating dictionaries...')
    create_dictionaries('public_html/js/dictionaries.js')
    print('Done!')
